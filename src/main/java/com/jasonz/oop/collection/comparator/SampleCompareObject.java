package com.jasonz.oop.collection.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SampleCompareObject
{
	List<YourBean> lstBeans = new ArrayList<>();

	public static void main(String[] args)
	{
		SampleCompareObject sco = new SampleCompareObject();
		sco.testComparator();
	}

	public void testComparator()
	{
		YourBean b1 = new YourBean();
		lstBeans.add(b1);
		// .....
		// .....
		Collections.sort(lstBeans, new BeanComparator());
	}

	private class BeanComparator implements Comparator<YourBean>
	{
		public int compare(YourBean a, YourBean b)
		{
			// -1
			// 0
			// 1;
			return 0;
		}
	}

}