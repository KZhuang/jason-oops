package com.jasonz.oop.abstraction;

public class MyImplementB extends MyImplementA
{

	public void b()
	{
		System.out.println("I am b in a concrete class");

	}

	public void c()
	{
		System.out.println("I am c in a concrete class");

	}

	public void d()
	{
		System.out.println("I am d in a concrete class" );

	}
	

}
